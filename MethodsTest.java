import java.util.Scanner;

public class MethodsTest{
	public static void main(String[]args){
		
		int x=5;
		int z=methodNoInputReturnInt();
	
		methodOneInputNoReturn(x+10);
		
		methodTwoInputNoReturn(x, 3.5);
		
		System.out.println("num " + z);
		
		double y=sumSquareRoot(9,5);
		
		System.out.println("square root is " + y);
		
		String s1="Java";
		String s2="programming";
		
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc= new SecondClass();
		
		System.out.println(SecondClass.addOne(50));
		
		System.out.println(sc.addTwo(50));
		
		
		
	}
	
	public static void methodNoInputNoReturn(){
		int x=20;
		
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int anyNum){
		
		anyNum=anyNum-5;
		System.out.println("inside the method one input no return " + anyNum);
		
	}
	
	public static void methodTwoInputNoReturn(int anyNum,  double anyDouble){
		System.out.println("int is :" + anyNum);
		System.out.println("double is :" + anyDouble);
	}
	
	public static int methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int num1, int num2){
		double num=0;
		
		num=num1+num2;
		
		num= Math.sqrt(num);
		
		
		return num;
	}
		
	
}