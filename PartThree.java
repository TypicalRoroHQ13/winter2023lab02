import java.util.Scanner;

public class PartThree{
	public static void main(String[]args){
		Scanner reader=new Scanner(System.in);
	
		System.out.println("Enter your 1st number");
		double num1= reader.nextDouble();
		
		System.out.println("Enter your 2nd number");
		double num2= reader.nextDouble();
		
		Calculator ca= new Calculator();
		
		System.out.println(Calculator.add(num1, num2));
		System.out.println(ca.subtract(num1, num2));
		System.out.println(Calculator.multiply(num1, num2));
		System.out.println(ca.divide(num1, num2));
		
	}
}